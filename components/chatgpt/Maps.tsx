import { useState } from "react";
import { ComposableMap, Geographies, Geography } from "react-simple-maps";
import worldGeoData from "./world-geo.json"; // GeoJSON data for world map

const populationData = [
  { id: "USA", name: "United States", population: 331002651 },
  { id: "CAN", name: "Canada", population: 37742154 },
  // Add population data for other countries...
];

const Map = () => {
  const [hoveredCountry, setHoveredCountry] = useState(null);

  const handleCountryHover = (geography) => {
    setHoveredCountry(geography.properties.ISO_A3);
  };

  const handleCountryLeave = () => {
    setHoveredCountry(null);
  };

  return (
    <div>
      <ComposableMap
        projection="geoMercator"
        projectionConfig={{ scale: 100 }}
        width={800}
        height={400}
      >
        <Geographies geography={worldGeoData}>
          {({ geographies }) =>
            geographies.map((geo) => (
              <Geography
                key={geo.rsmKey}
                geography={geo}
                onMouseEnter={() => handleCountryHover(geo)}
                onMouseLeave={handleCountryLeave}
                style={{
                  default: {
                    fill:
                      hoveredCountry === geo.properties.ISO_A3
                        ? "red"
                        : "#D6D6DA",
                    stroke: "#FFF",
                    strokeWidth: 0.5,
                    outline: "none",
                  },
                  hover: {
                    fill: "red",
                    stroke: "#FFF",
                    strokeWidth: 0.5,
                    outline: "none",
                  },
                }}
              />
            ))
          }
        </Geographies>
      </ComposableMap>
    </div>
  );
};

export default Map;

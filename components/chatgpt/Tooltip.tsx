const Tooltip = ({ name, population, x, y }) => {
  return (
    <div
      style={{
        position: "absolute",
        left: x,
        top: y,
        background: "white",
        padding: "5px",
      }}
    >
      <div>{name}</div>
      <div>Population: {population}</div>
    </div>
  );
};

export default Tooltip;

import { useEffect, useRef } from "react";
import * as d3 from "d3";

const WorldMap = () => {
  const svgRef = useRef(null);

  const width = 800;
  const height = 500;

  useEffect(() => {
    const svg = d3.select(svgRef.current);

    const projection = d3
      .geoMercator()
      .translate([width / 2, height / 2])
      .scale(160);

    const pathGenerator = d3.geoPath().projection(projection);

    // Load world topojson data
    d3.json("components/data/world-110m.json").then((worldData) => {
      // Draw countries
      svg
        .append("g")
        .selectAll("path")
        .data(worldData.features)
        .enter()
        .append("path")
        .attr("d", pathGenerator)
        .style("fill", "lightgrey")
        .style("stroke", "white");

      // Load country population data
      d3.csv("countries.csv").then((data) => {
        // Draw tooltip on hover
        svg
          .selectAll("path")
          .on("mouseover", (d, i, nodes) => {
            d3.select(nodes[i]).style("fill", "orange");

            const name = data.find((c) => c.id === d.id).name;
            const population = data.find((c) => c.id === d.id).population;

            // svg
            //   .append("text")
            //   .text(`${name} - ${population}`)
            //   .attr("x", event.pageX)
            //   .attr("y", event.pageY);
          })
          .on("mouseout", (d, i, nodes) => {
            d3.select(nodes[i]).style("fill", "lightgrey");

            svg.select(".tooltip").remove();
          });
      });
    });
  }, []);

  return <svg ref={svgRef} width={500} height={300}></svg>;
};

export default WorldMap;

import React from "react";
import Map from "../../components/chatgpt/Maps";
import Tooltip from "../../components/chatgpt/Tooltip";

const App = () => {
  return (
    <div>
      <h1>Interactive World Map</h1>
      <Map />
      <Tooltip name="United States" population="331,002,651" x={50} y={50} />
    </div>
  );
};

export default App;
